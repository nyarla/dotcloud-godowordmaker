package GodowordMaker::Converter::yukio;

use strict;
use warnings;
use utf8;

use Lingua::JA::Moji qw/ hira2kata /;

my %chars    = qw/
    カ ガ
    キ ギ
    ク グ
    ケ ゲ
    コ ゴ
    サ ザ
    シ ジ
    ス ズ
    セ ゼ
    ソ ゾ
    タ ダ
    チ ヂ
    ツ ヅ
    テ デ
    ト ド
    ハ バ
    ヒ ビ
    フ ブ
    ヘ ベ
    ホ ボ
/;

sub convert {
    my ( $class, $message ) = @_;

    $message = hira2kata( $message );
    
    for my $target ( keys %chars ) {
        $message =~ s/${target}/$chars{$target}/gs;
    }

    return $message;
}

1;

