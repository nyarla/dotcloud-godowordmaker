package GodowordMaker::Converter::godoword;

use strict;
use warnings;

use Acme::GodoWord;

sub convert {
    my ( $class, $message ) = @_;
    return Acme::GodoWord->babelize( $message );
}

1;
