package GodowordMaker;

use strict;
use warnings;

our $VERSION = 0.01;

use Dancer ':syntax';

set public => path( dirname(__FILE__), '..', 'static' );

get '/' => sub {
    template 'index', { uri_for => \&uri_for };
};

get '/generate' => sub {
    redirect uri_for('/');
};

my %converters = map { $_ => "GodowordMaker::Converter::${_}" } qw/ godoword yukio /;

post '/generate' => sub {
    my $converter = params->{'converter'};
    my $message   = params->{'message'};
       $message   =~ s{\r?\n}{}gs;

    if ( exists $converters{$converter} ) {
        my $class = $converters{$converter};
        eval "require ${class}";

        return template 'publish', { message => $class->convert($message) };
    }
    else {
        return template 'error';
    }
};

true;

