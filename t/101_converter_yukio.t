#!/usr/bin/env perl

use strict;
use warnings;
use utf8;

use Test::More;

BEGIN {
    use_ok('GodowordMaker::Converter::yukio');
};

my $source = q{助けてよう、せんぱーい};

is(
    GodowordMaker::Converter::yukio->convert($source),
    q{助ゲデヨウ、ゼンパーイ},
);

done_testing;

