#!/usr/bin/env perl

use strict;
use warnings;
use utf8;

use Test::More;

BEGIN {
    use_ok('GodowordMaker::Converter::godoword');
}

my $source = q{あなたには見えない};

is(
    GodowordMaker::Converter::godoword->convert($source),
    q{【あなた」「には」「見えない】},
);

done_testing;
